import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-burger-display',
  templateUrl: './burger-display.component.html',
  styleUrls: ['./burger-display.component.scss']
})
export class BurgerDisplayComponent implements OnInit {
  @Input('Ingredients') ing_list: string[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}

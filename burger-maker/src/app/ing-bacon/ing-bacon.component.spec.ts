import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngBaconComponent } from './ing-bacon.component';

describe('IngBaconComponent', () => {
  let component: IngBaconComponent;
  let fixture: ComponentFixture<IngBaconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngBaconComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IngBaconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Adding } from '../ing-meat/ing-meat.component';

@Component({
  selector: 'app-ing-bacon',
  templateUrl: './ing-bacon.component.html',
  styleUrls: ['./ing-bacon.component.scss']
})
export class IngBaconComponent implements OnInit {
  @Input() bacon: number = 0;
  @Output() newBacon = new EventEmitter<Adding>();

  constructor() { }

  ngOnInit(): void {
  }

  handlerBacon($event: any): void {
    const action: string = $event.target.dataset.id;
    switch (action) {
      case "add":
        this.bacon += 1;
        break;
      case "remove":
        if (this.bacon === 0) break;
        this.bacon -= 1;
        break;
      default:
        break;
    }   
    this.newBacon.emit({ing_type: 'bacon', ing_action: action, ing_quan: this.bacon});
  }

}

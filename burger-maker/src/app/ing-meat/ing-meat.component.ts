import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-ing-meat',
  templateUrl: './ing-meat.component.html',
  styleUrls: ['./ing-meat.component.scss']
})
export class IngMeatComponent implements OnInit {
  @Input() meat: number = 0;
  @Output() newMeat = new EventEmitter<Adding>();

  constructor() { }

  ngOnInit(): void {
  }

  handlerMeat($event: any): void {
    const action: string = $event.target.dataset.id;
    switch (action) {
      case "add":
        this.meat += 1;
        break;
      case "remove":
        if (this.meat === 0) break;
        this.meat -= 1;
        break;
      default:
        break;
    }
    this.newMeat.emit({ing_type: 'meat', ing_action: action, ing_quan: this.meat});
  }
}

export interface Adding {
  ing_type: string,
  ing_action: string,
  ing_quan: number
}
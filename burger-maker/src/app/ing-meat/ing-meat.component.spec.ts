import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngMeatComponent } from './ing-meat.component';

describe('IngMeatComponent', () => {
  let component: IngMeatComponent;
  let fixture: ComponentFixture<IngMeatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngMeatComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IngMeatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Adding } from '../ing-meat/ing-meat.component';

@Component({
  selector: 'app-main-display',
  templateUrl: './main-display.component.html',
  styleUrls: ['./main-display.component.scss']
})
export class MainDisplayComponent implements OnInit {
  totalPrice: number = 1000.0;

  actualCurrency: string = 'usd'

  currency: any = {
    meat: {
      usd: 1500,
      aud: 2310,
      mxn: 29970
    },
    cheese: {
      usd: 500,
      aud: 770,
      mxn: 10000
    },
    salad: {
      usd: 250,
      aud: 380,
      mxn: 5000
    },
    bacon: {
      usd: 500,
      aud: 770,
      mxn: 10000
    }
  }

  currencyValues: string[] = [
    'usd',
    'aud',
    'mxn'
  ]
  
  recipeQuan: RecipeQuan = {
    meat: 0,
    cheese: 0,
    salad: 0,
    bacon: 0
  }

  recipeOrder: string[] = [];

  burgerList: Burger[] = JSON.parse(localStorage.getItem('BurgerList') as string) || []

  savedBurger: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  addIng(ingredients: Adding): void{
    const type: string = ingredients.ing_type;
    const action: string = ingredients.ing_action;
    switch (type) {
      case 'meat':
        this.recipeQuan['meat'] = ingredients.ing_quan;
        this.handleBurgerIng(type, action);
        this.handleBurgerPrice(type, action);
        break;
      case 'cheese':
        this.recipeQuan['cheese'] = ingredients.ing_quan;
        this.handleBurgerIng(type, action);
        this.handleBurgerPrice(type, action);
        break;
      case 'salad':
        this.recipeQuan['salad'] = ingredients.ing_quan;
        this.handleBurgerIng(type, action);
        this.handleBurgerPrice(type, action);
        break;
      case 'bacon':
        this.recipeQuan['bacon'] = ingredients.ing_quan;
        this.handleBurgerIng(type, action);
        this.handleBurgerPrice(type, action);
        break;
      default:
        break;
    }
  }

  handleBurgerPrice(type: string, action: string) {
    if (action === 'add') {
      this.totalPrice += this.currency[type][this.actualCurrency];
    }else {
      this.totalPrice -= this.currency[type][this.actualCurrency];
    }
  }

  handleBurgerIng(type: string, action: string ) {
    const ing_index: number = this.recipeOrder.indexOf(type);
    if (action === 'add') {
      this.recipeOrder.unshift(type);
    }else {
      this.recipeOrder.splice(ing_index, 1);
    }
  }

  handleCurrency(currency: string) {
    switch (currency) {
      case 'usd':
        if (this.actualCurrency === 'aud') {
          this.totalPrice /= 1.54;
        }else{
          this.totalPrice /= 19.98;
        }
        this.actualCurrency = 'usd';
        break;
      case 'aud':
        if (this.actualCurrency === 'usd') {
          this.totalPrice *= 1.54;
        }else {
          this.totalPrice /= 12.99;
        }
        this.actualCurrency = 'aud';
        break;
      case 'mxn':
        if (this.actualCurrency === 'usd') {
          this.totalPrice *= 19.98;
        }else{
          this.totalPrice *= 12.99;
        }
        this.actualCurrency = 'mxn';
        break;
      default:
        break;
    }

  }

  saveThisBurger(): void {
    let recipeBurger: Burger = {
      recipe: {...this.recipeQuan},
      order: [...this.recipeOrder],
      totalPrice: this.totalPrice,
      currency: this.actualCurrency
    };
    this.burgerList.push(recipeBurger)
    localStorage.setItem('BurgerList', JSON.stringify(this.burgerList));
  }

  getThisBurger(index: string): void {
    const data: Burger[] = JSON.parse(localStorage.getItem('BurgerList') as string)
    this.totalPrice = data[+index].totalPrice;
    this.recipeOrder = data[+index].order;
    this.recipeQuan = data[+index].recipe;
    this.actualCurrency = data[+index].currency;
    this.savedBurger = index;
  }

  updateThisBurger(): void {
    const index = +this.savedBurger;
    this.burgerList[+index].totalPrice = this.totalPrice;
    this.burgerList[+index].order = [...this.recipeOrder];
    this.burgerList[+index].recipe = {...this.recipeQuan};
    this.burgerList[+index].currency = this.actualCurrency;
    localStorage.setItem('BurgerList', JSON.stringify(this.burgerList))
  }
}

export interface RecipeQuan {
  meat: number,
  cheese: number,
  salad: number,
  bacon: number
}

interface Burger {
  recipe: RecipeQuan,
  order: string[],
  totalPrice: number,
  currency: string
}


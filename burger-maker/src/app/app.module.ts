import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainDisplayComponent } from './main-display/main-display.component';
import { IngMeatComponent } from './ing-meat/ing-meat.component';
import { IngCheeseComponent } from './ing-cheese/ing-cheese.component';
import { IngSaladComponent } from './ing-salad/ing-salad.component';
import { IngBaconComponent } from './ing-bacon/ing-bacon.component';
import { BurgerDisplayComponent } from './burger-display/burger-display.component';
import { IngTableComponent } from './ing-table/ing-table.component';

@NgModule({
  declarations: [
    AppComponent,
    MainDisplayComponent,
    IngMeatComponent,
    IngCheeseComponent,
    IngSaladComponent,
    IngBaconComponent,
    BurgerDisplayComponent,
    IngTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

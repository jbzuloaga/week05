import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngSaladComponent } from './ing-salad.component';

describe('IngSaladComponent', () => {
  let component: IngSaladComponent;
  let fixture: ComponentFixture<IngSaladComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngSaladComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IngSaladComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Adding } from '../ing-meat/ing-meat.component';

@Component({
  selector: 'app-ing-salad',
  templateUrl: './ing-salad.component.html',
  styleUrls: ['./ing-salad.component.scss']
})
export class IngSaladComponent implements OnInit {
  @Input() salad: number = 0;
  @Output() newSalad = new EventEmitter<Adding>();
  
  constructor() { }

  ngOnInit(): void {
  }


  handlerSalad($event: any): void {
    const action: string = $event.target.dataset.id;
    switch (action) {
      case "add":
        this.salad += 1;
        break;
      case "remove":
        if (this.salad === 0) break;
        this.salad -= 1;
        break;
      default:
        break;
    }
    this.newSalad.emit({ing_type: 'salad', ing_action: action, ing_quan: this.salad});
  }

}

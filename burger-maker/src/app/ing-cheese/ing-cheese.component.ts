import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Adding } from '../ing-meat/ing-meat.component';

@Component({
  selector: 'app-ing-cheese',
  templateUrl: './ing-cheese.component.html',
  styleUrls: ['./ing-cheese.component.scss']
})
export class IngCheeseComponent implements OnInit {
  @Input() cheese: number = 0;
  @Output() newCheese = new EventEmitter<Adding>();

  constructor() { }

  ngOnInit(): void {
  }

  handlerCheese($event: any): void {
    const action: string = $event.target.dataset.id;
    switch (action) {
      case "add":
        this.cheese += 1;
        break;
      case "remove":
        if (this.cheese === 0) break;
        this.cheese -= 1;
        break;
      default:
        break;
    }
    this.newCheese.emit({ing_type: 'cheese', ing_action: action, ing_quan: this.cheese});
  }

}

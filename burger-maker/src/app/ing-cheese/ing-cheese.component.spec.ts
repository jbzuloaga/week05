import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngCheeseComponent } from './ing-cheese.component';

describe('IngCheeseComponent', () => {
  let component: IngCheeseComponent;
  let fixture: ComponentFixture<IngCheeseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngCheeseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IngCheeseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { RecipeQuan } from '../main-display/main-display.component';

@Component({
  selector: 'app-ing-table',
  templateUrl: './ing-table.component.html',
  styleUrls: ['./ing-table.component.scss']
})
export class IngTableComponent implements OnInit {
  @Input() recipeQuan: RecipeQuan = {
    meat: 0,
    cheese: 0,
    salad: 0,
    bacon: 0
  };
  @Input() totalPrice: number = 0; 
  @Input() currency: any;
  @Input() actualCurrency: string = ''; 

  constructor() { }

  ngOnInit(): void {
  }

}

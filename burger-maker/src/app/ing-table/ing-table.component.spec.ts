import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngTableComponent } from './ing-table.component';

describe('IngTableComponent', () => {
  let component: IngTableComponent;
  let fixture: ComponentFixture<IngTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IngTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
